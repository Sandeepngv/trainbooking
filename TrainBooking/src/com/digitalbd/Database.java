package com.digitalbd;

import java.sql.*;

public class Database {
	private String hostName = "jdbc:oracle:thin:@localhost:1521:xe";
	private String userName = "db";
	private String userPassword = "123";
	public Statement statement;
	private Connection con;

	public Database() {
		this.con = null;
		this.statement = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			this.con = DriverManager.getConnection(hostName, userName, userPassword);
			this.statement = this.con.createStatement();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
