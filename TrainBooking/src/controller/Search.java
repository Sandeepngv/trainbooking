package controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.*;
import java.sql.*;
import java.util.*;

public class Search extends HttpServlet{ 
 
 public void doPost(HttpServletRequest request, 
  HttpServletResponse response)
  throws ServletException,IOException{
  response.setContentType("text/html");
  PrintWriter out = response.getWriter();

  System.out.println("MySQL Connect Example.");
  Connection conn = null;
  String url = "jdbc:oracle:thin:@localhost:1521:xe";
  String driver = "oracle.jdbc.driver.OracleDriver";
  String userName = "db"; 
  String password = "123";

  
  Statement st;
  try {
  Class.forName(driver).newInstance();
  conn = DriverManager.getConnection(url+url,userName,password);
  System.out.println("Connected to the database");
  String  stationfrom  = request.getParameter("stationfrom");
  String  stationto  = request.getParameter("stationto");
  String  train_number  = request.getParameter("train_number");

  ArrayList al=null;
  ArrayList emp_list =new ArrayList();
  String query = 
  "select * from search where stationfrom='"+stationfrom+"' or stationto'"+stationto+"' or train_number='"+train_number+"' order by stationfrom";
  System.out.println("query " + query);
  st = conn.createStatement();
  ResultSet  rs = st.executeQuery(query);


  while(rs.next()){
  al  = new ArrayList();
  
  al.add(rs.getString(1));
  al.add(rs.getString(2));
  al.add(rs.getInt(3));
  al.add(rs.getString(4));
  al.add(rs.getString(5));
  al.add(rs.getString(6));
  al.add(rs.getString(7));
  al.add(rs.getInt(8));
  al.add(rs.getString(9));
  System.out.println("al :: "+al);
  emp_list.add(al);
  }

  request.setAttribute("empList",emp_list);
  
 System.out.println("empList " + emp_list);

  // out.println("emp_list " + emp_list);

  String nextJSP = "/viewSearch.jsp";
  RequestDispatcher dispatcher = 
   getServletContext().getRequestDispatcher(nextJSP);
  dispatcher.forward(request,response);
  conn.close();
  System.out.println("Disconnected from database");
  } catch (Exception e) {
  e.printStackTrace();
  }
  }
}
